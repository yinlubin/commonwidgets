package com.common.widgets;

import lubiny.liwidgets.R;

public class Attrs {

    public static final int [] EditTextWithButton_Params = new int[] {
        R.attr.inputName,
        R.attr.inputNameTextSize,
        R.attr.inputNameVisible,
        R.attr.inputTextSize,
        R.attr.inputId,
        R.attr.buttonBackgound,
        R.attr.buttonVisiable,
        R.attr.inputHint,
        R.attr.inputTextColor,
        R.attr.maxLength,
        R.attr.isAlipayMoney,
//        R.attr.inputType,
        android.R.attr.inputType
    };

    public static final int InputName = 0;
    public static final int InputNameTextSize = 1;
    public static final int InputNameVisible = 2;
    public static final int InputTextSize = 3;
    public static final int InputId = 4;
    public static final int ButtonBackgound = 5;
    public static final int ButtonVisiable = 6;
    public static final int InputHint = 7;
    public static final int InputTextColor = 8;
    public static final int MaxLength = 9;
    public static final int IsAlipayMoney = 10;
    public static final int InputType = 11;

}
