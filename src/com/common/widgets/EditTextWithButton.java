
package com.common.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import lubiny.liwidgets.R;

public class EditTextWithButton extends RelativeLayout
{
    private EditText content;
    private ImageButton clearButton;
    // private ImageButton funcButton;
    private TextView contentName;
    private String inputNameS;
    private float inputNameTextSize;
    private boolean inputNameVisible;
    private float inputTextSize;
    private int inputTextColor;
    private int inputType;
    private int inputId;
    private Drawable buttonBackground;
    private int maxLength;
    private String inputHint;
    private boolean buttonVisible;

    // private boolean isAlipayMoney;

    public EditTextWithButton(Context paramContext)
    {
        super(paramContext);
    }

    public EditTextWithButton(Context paramContext, AttributeSet paramAttributeSet)
    {
        super(paramContext, paramAttributeSet);
        LayoutInflater.from(paramContext).inflate(R.layout.common_edittext_with_button, this, true);
        TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet,
                Attrs.EditTextWithButton_Params);
        this.inputNameS = localTypedArray.getString(Attrs.InputName);
        this.inputNameTextSize = localTypedArray.getFloat(Attrs.InputNameTextSize, 16.0F);
        this.inputNameVisible = localTypedArray.getBoolean(Attrs.InputNameVisible, true);
        this.inputTextSize = localTypedArray.getFloat(Attrs.InputTextSize, 16.0F); // ?
        this.inputTextColor = localTypedArray.getColor(Attrs.InputTextColor, -16777216);
        this.inputType = localTypedArray.getInt(Attrs.InputType, InputType.TYPE_CLASS_TEXT);
        this.maxLength = localTypedArray.getInt(Attrs.MaxLength, -1);
        this.inputId = localTypedArray.getInt(Attrs.InputId, 0);
        this.inputHint = localTypedArray.getString(Attrs.InputHint);
        this.buttonBackground = localTypedArray.getDrawable(Attrs.ButtonBackgound);
        this.buttonVisible = localTypedArray.getBoolean(Attrs.ButtonVisiable, false);
        // this.isAlipayMoney = localTypedArray.getBoolean(Attrs.IsAlipayMoney, false);
        localTypedArray.recycle();
    }

    public final EditText getContentControl() {
        return content;
    }

    public final String getContent()
    {
        return this.content.getText().toString();
    }

    public final void setContent(CharSequence paramCharSequence)
    {
        this.content.setText(paramCharSequence);
    }

    public final void setContentName(String paramString)
    {
        if (!TextUtils.isEmpty(paramString))
        {
            this.contentName.setText(paramString);
            this.contentName.setVisibility(0);
        }
    }

    public final void showClearButton(boolean paramBoolean) // ?
    {
        if (paramBoolean)
        {
            this.clearButton.setVisibility(View.VISIBLE);
        }
        // funcButton.setVisibility(View.GONE);
    }

    @Override
    protected void onFinishInflate()
    {
        super.onFinishInflate();
        this.content = ((EditText) findViewById(R.id.content));
        this.contentName = ((TextView) findViewById(R.id.contentName));
        this.clearButton = ((ImageButton) findViewById(R.id.clearButton));
        // this.funcButton = ((ImageButton) findViewById(R.id.funcButton));
        setContentName(this.inputNameS);
        this.contentName.setVisibility(this.inputNameVisible? View.VISIBLE : View.GONE);
        if (this.inputNameTextSize > 0.0F)
            this.contentName.setTextSize(this.inputNameTextSize);
        if (this.inputTextSize > 0.0F)
            this.content.setTextSize(this.inputTextSize);
        this.content.setTextColor(this.inputTextColor);
        if (this.inputId != 0)
            this.content.setId(this.inputId);
        this.content.setInputType(this.inputType);
        if (!TextUtils.isEmpty(inputHint))
            this.content.setHint(this.inputHint);
        if (this.maxLength >= 0)
        {
            EditText localEditText = this.content;
            InputFilter[] arrayOfInputFilter = new InputFilter[1];
            arrayOfInputFilter[0] = new InputFilter.LengthFilter(this.maxLength);
            localEditText.setFilters(arrayOfInputFilter);
        }
        while (true)
        {
            this.content.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable paramEditable) {
                    if (TextUtils.isEmpty(paramEditable.toString())) {
                        showClearButton(false);
                    } else {
                        showClearButton(true);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }
            });

            this.clearButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    EditTextWithButton.this.content.setText("");
                    // EditTextWithButton.this.contentName.setVisibility(View.GONE);
                }
            });

            showClearButton(this.buttonVisible);
            Drawable localDrawable = this.buttonBackground;
            if (localDrawable != null)
            {
                this.buttonBackground = localDrawable;
                // if (this.buttonBackground != null)
                // this.funcButton.setVisibility(View.VISIBLE);
                this.clearButton.setVisibility(View.GONE);
                // this.funcButton.setBackgroundDrawable(localDrawable);
            }
            return;
        }
    }
}
